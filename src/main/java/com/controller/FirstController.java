package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/firstController")
public class FirstController {
    /*请求转发第一种方式
     * @author yh
     * */
    @RequestMapping("/one")
    public ModelAndView one() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("firstview");
        mv.addObject("username", "yh");
        return mv;
    }

    /*请求转发第二种方式
     * @author yh
     * */
    @RequestMapping("/tow")
    public String tow(HttpServletRequest request) {
        String username = request.getParameter("username");
        String id = request.getParameter("id");
        System.out.println(username);
        System.out.println(id);
        return "forward:/secondview.jsp";
    }
}
