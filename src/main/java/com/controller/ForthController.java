package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("forthController")
public class ForthController {
    @RequestMapping(value = "/{username}/*/{password}", method = RequestMethod.POST)
    @ResponseBody
    public void one(@PathVariable String username, @PathVariable String password) {
        System.out.println(username);
        System.out.println(password);

    }
}
