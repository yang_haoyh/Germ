package com.controller;

import com.bean.DogBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/requestJsonController")
public class RequestJsonController {
    @RequestMapping("/one")
    @ResponseBody
    public void one(DogBean dogBean){
        System.out.println(dogBean.getDog_color());
        System.out.println(dogBean.getDog_id());
        System.out.println(dogBean.getDog_name());
    }
}
