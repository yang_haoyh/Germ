package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/secondController")
public class SecondController {
    /*重定向的modelandview方式
     * @author yh
     * */
    @Autowired
    HttpServletRequest request;
    @RequestMapping("/one")
    public ModelAndView one (){
        String username =request.getParameter("username");
        String id = request.getParameter(("id"));
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/secondview.jsp?username="+username+"&id="+id+"");
        return mv;
    }
    @RequestMapping("/tow")
    /*重定向的String方式
     * @author yh
     * */
    public String tow(){
        String username =request.getParameter("username");
        String id = request.getParameter(("id"));
        System.out.println(username);
        System.out.println(id);
        return "redirect:/secondview.jsp?sno=3188908311";
    }
}
