package com.controller;

import com.bean.DogBean;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@Controller
@RequestMapping("/responseJsonController")
public class ResponseJsonController {
//    @Autowired
//    private HttpServletResponse response;
    @Autowired
    private HttpServletRequest request;
    @RequestMapping("/one")
    @ResponseBody
    public void one()  {
        DogBean dogBean = new DogBean();
        dogBean.setDog_name("皮皮");
        dogBean.setDog_id(001);
        dogBean.setDog_color("red");
        System.out.println(dogBean.getDog_name());
        System.out.println(dogBean.getDog_id());
        System.out.println(dogBean.getDog_color());
        Gson gson = new Gson();
        String str = gson.toJson(dogBean);
        System.out.println(str);
//        PrintWriter out = null;
//        try {
//            out = response.getWriter();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        out.print(str);
//        out.flush();
//        out.close();

    }
}
