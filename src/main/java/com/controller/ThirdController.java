package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/thirdController" )
public class ThirdController {
    @RequestMapping("/one")
    @ResponseBody
    public void one(@RequestParam(value = "username") String name,@RequestParam(value = "id",required = false) String id) {
        System.out.println(name);
        System.out.println(id);
    }
}
