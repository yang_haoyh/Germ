package com.bean;

public class DogBean {
    private String dog_name;
    private int dog_id;
    private String dog_color;

    public DogBean() {
    }

    public String getDog_name() {
        return this.dog_name;
    }

    public void setDog_name(String dog_name) {
        this.dog_name = dog_name;
    }

    public int getDog_id() {
        return this.dog_id;
    }

    public void setDog_id(int dog_id) {
        this.dog_id = dog_id;
    }

    public String getDog_color() {
        return this.dog_color;
    }

    public void setDog_color(String dog_color) {
        this.dog_color = dog_color;
    }
}
