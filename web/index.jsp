<%--
  Created by IntelliJ IDEA.
  User: yh
  Date: 2020/4/24
  Time: 17:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE-edge,chrome-1">
    <title>Title</title>
</head>
<body>
<pre>
    <a href="${pageContext.request.contextPath}/firstController/one">请求转发第一种方式</a>
    <a href="${pageContext.request.contextPath}/firstController/tow?username=yh&id=1">请求转发第二种方式</a>
    <a href="${pageContext.request.contextPath}/secondController/one?username=yh&id=1">重定向第一种方式</a>
    <a href="${pageContext.request.contextPath}/secondController/tow?username=yh&id=1">重定向第二种方式</a>
    <a href="${pageContext.request.contextPath}/thirdController/one?username=yh">Spring传参</a>
    <img src="${pageContext.request.contextPath}/images/one.png">
</pre>
<pre>

    Springmvc中对json的操作:
    <input type="button" onclick="ajax_response_one()" value="转换方式">
</pre>
<form action="" id="form">
    dog_id<input type="text" name="dog_id" value=1>
    dog_name<input type="text" name="dog_name" value="1">
    dog_color<input type="text" name="dog_color" value="red">
    <input type="button" onclick="ajax_request_one()" value="发送表单数据">
</form>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
    function ajax_response_one() {
        var url = "${pageContext.request.contextPath}/responseJsonController/one?date=" + Math.random() + ""
        jQuery.get(url, function (jsonData) {
            window.alert(jsonData);
        }, "json")
    }
    function ajax_request_one(){
        var url="${pageContext.request.contextPath}/requestJsonController/one?date=" + Math.random() + ""
            $.ajax({
                type : "post",
                url : url,
                data : $("#form").serialize(),
                dataType : "json",
                success : function(dogBean) {
                    window.alert(dogBean)
                    }
        });
    };

</script>
</body>
</html>
